MXParse
=======
A mathematical expression parser and REPL for the JVM written to demonstrate lexer/parser concepts.

Over the years of teaching computer science, I've had a couple students ask how they might create a
calculator in Java. While this project started as a way to introduce lexers and parsers to those
students, it has since become a platform for me to improve my own understanding. In order for the
project to be accessible for intro comp sci students, I originally made the following choices:

- Hard-coded the grammar into the lexer and parsers.
- Wrote the lexer without using regular expressions, as they weren't part of the curriculum.
- Used the relatively simple
  [shunting yard algorithm](https://en.wikipedia.org/wiki/Shunting_yard_algorithm) to convert infix
  notation expressions into reverse polish notation (RPN), so that they could be easily parsed.

I have changed all of these things, so the project may no longer be a good introduction for people
early on in their education. However, the benefits are great:

- Lexers and parsers can easily be created for a wider variety of grammars and expression types.
- Lexer implementation is much simpler and more concise by using regexes.
- The shunting yard algorithm makes it difficult to support unary operators that share a token with
  a binary operator (e.g., unary minus and binary minus both using `-`) because of its dependence on
  an RPN parser. The RPN parser has no way to disambiguate between unary/binary versions of an 
  operator, so something like `-5` would have to be converted into `0 5 -`. As a result, the
  shunting yard algorithm has been replaced with a
  [Pratt parser](https://en.wikipedia.org/wiki/Operator-precedence_parser#Pratt_parsing), which can
  easily handle prefix, infix, and postfix operators (but arguably takes more time to understand).

Getting Started
---------------

### REPL
- Linux/macOS: `sh ./gradlew run`
- Windows: `.\gradlew.bat run`

The REPL defaults to parsing infix notation, and supports the following:
- Integer literals
- Grouping with `(` and `)`
- Unary prefix operators:
  - Plus (`+`)
  - Minus (`-`)
  - Bitwise complement (`~`)
- Unary postfix operators:
  - Factorial (`!`)
- Binary infix operators:
  - Exponentiation (`**`)
  - Times (`*`)
  - Divide (`/`)
  - Modulo (`%`)
  - Plus (`+`)
  - Minus (`-`)
  - Bitwise AND (`&`)
  - Bitwise XOR (`^`)
  - Bitwise OR (`|`)

Whitespace is ignored.

### Library
Both the `PrattParser` (used for infix notation) and `PostfixParser` (used for reverse polish
notation) are generically typed and can be used to parse an expression that evaluates to any type of
data. However, they do not support mixing types - for example, they will not be able to handle an
expression like `3 < 5`.

1. Create a `Token.Type` instance for each kind of token that exists in your grammar:
   ```java
   Token.Type integer = new Token.Type("Integer"); 
   ```
   Many types common to mathematical expressions have already been defined as static members of the
   `Token.Type` class.
2. Create a `Lexer` instance, giving it a regex for each token type it should recognize:
   ```java
   Lexer lexer = new Lexer(Map.of(integer, "\\d+"));
   ```
3. Create a `PrattParser` or `PostfixParser` instance using their respective `Builder` nested class:
   ```java
   PrattParser<Integer> parser = new PrattParser.Builder<Integer>()
       .withLiteral(integer, Integer::parseInt)
       .instance();
   ```
4. Tokenize a string with the `Lexer`, and parse the result into an expression:
   ```java
   Iterable<Token> tokens = lexer.tokenize("9001");
   Expression<Integer> expression = parser.parse(tokens);
   ```
5. Evaluate the expression:
   ```java
   int result = expression.evaluate();
   ```

The parser builder classes have many more methods available for adding common token archetypes
(e.g., prefix operators, postfix operators, infix operators, groupings).
