package com.scotthamper.mxparse.repl;

import com.scotthamper.mxparse.Expression;
import com.scotthamper.mxparse.Lexer;
import com.scotthamper.mxparse.PostfixParser;
import com.scotthamper.mxparse.PrattParser;
import com.scotthamper.mxparse.Token;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Main {
  // Operator precedence for an exponentiation operator can be surprisingly ambiguous. For example,
  // in traditional math notation (where an exponent is written in superscript), my sense is that
  // most people would evaluate an expression like "negative two squared" as equalling negative
  // four. In other words, the exponentiation would take precedence over the unary minus. However,
  // when writing an expression in plaintext, we cannot write the exponent in superscript. Instead,
  // exponentiation is written using an explicit operator, like `**`. Given that many programming
  // languages have a convention of surrounding binary operators with spaces, but not unary
  // operators, it makes it easy to perceive the `-2` as being implicitly grouped (see Gestalt
  // Principles of Perception).
  //
  // Whether for this reason or others, it turns out that there is no universal consensus among
  // programming languages for how to handle an expression like `-2 ** 2`. Some have the
  // exponentiation take precedence, others have the unary minus take precedence. JavaScript decides
  // it wants nothing to do with the situation - when it sees an unparenthesized unary operation on
  // the left-hand side of an exponentiation, it spits out an error.
  //
  // The conclusion I draw from this is that I get to make whatever decision I want! In the example
  // above, I'm going to give the exponentiation operator precedence, so that it matches what people
  // generally seem to expect from the expression when written in traditional math notation.
  public static final PrattParser<BigInteger> NUMBER_PRATT_PARSER =
      new PrattParser.Builder<BigInteger>()
          .withLiteral(Token.Type.NUMBER, BigInteger::new)
          .withGrouping(Token.Type.GROUP_START, Token.Type.GROUP_END)
          .withPrefix(Token.Type.PLUS, 6, x -> x)
          .withPrefix(Token.Type.MINUS, 6, BigInteger::negate)
          .withPrefix(Token.Type.BITWISE_COMPLEMENT, 6, x -> x.xor(BigInteger.valueOf(-1)))
          .withPostfix(Token.Type.FACTORIAL, 7, MathFunction::factorial)
          .withRightInfix(Token.Type.EXPONENTIATION, 6, (b, x) -> b.pow(x.intValueExact()))
          .withInfix(Token.Type.TIMES, 5, BigInteger::multiply)
          .withInfix(Token.Type.DIVIDE, 5, BigInteger::divide)
          .withInfix(Token.Type.MODULO, 5, BigInteger::remainder)
          .withInfix(Token.Type.PLUS, 4, BigInteger::add)
          .withInfix(Token.Type.MINUS, 4, BigInteger::subtract)
          .withInfix(Token.Type.BITWISE_AND, 3, BigInteger::and)
          .withInfix(Token.Type.BITWISE_XOR, 2, BigInteger::xor)
          .withInfix(Token.Type.BITWISE_OR, 1, BigInteger::or)
          .ignoring(Token.Type.WHITESPACE)
          .instance();

  public static final PostfixParser<BigInteger> NUMBER_POSTFIX_PARSER =
      new PostfixParser.Builder<BigInteger>()
          .withLiteral(Token.Type.NUMBER, BigInteger::new)
          .withUnary(Token.Type.BITWISE_COMPLEMENT, x -> x.xor(BigInteger.valueOf(-1)))
          .withUnary(Token.Type.FACTORIAL, MathFunction::factorial)
          .withBinary(Token.Type.EXPONENTIATION, (b, x) -> b.pow(x.intValueExact()))
          .withBinary(Token.Type.TIMES, BigInteger::multiply)
          .withBinary(Token.Type.DIVIDE, BigInteger::divide)
          .withBinary(Token.Type.MODULO, BigInteger::remainder)
          .withBinary(Token.Type.PLUS, BigInteger::add)
          .withBinary(Token.Type.MINUS, BigInteger::subtract)
          .withBinary(Token.Type.BITWISE_AND, BigInteger::and)
          .withBinary(Token.Type.BITWISE_XOR, BigInteger::xor)
          .withBinary(Token.Type.BITWISE_OR, BigInteger::or)
          .ignoring(Token.Type.WHITESPACE)
          .instance();

  public static void main(String[] args) {
    var scanner = new Scanner(System.in, StandardCharsets.UTF_8);
    var lexer = new Lexer();

    while (true) {
      System.out.print("Enter expression: ");
      String input = scanner.nextLine().trim().toLowerCase();

      if (input.equals("quit") || input.equals("exit")) {
        break;
      }

      Iterable<Token> tokens = lexer.tokenize(input);
      Expression<BigInteger> expression = NUMBER_PRATT_PARSER.parse(tokens);

      System.out.println(expression.evaluate());
    }
  }
}
