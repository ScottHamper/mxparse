package com.scotthamper.mxparse.repl;

import java.math.BigInteger;

public class MathFunction {
  public static BigInteger factorial(BigInteger x) {
    if (x.signum() < 0) {
      throw new IllegalArgumentException();
    }

    BigInteger result = BigInteger.ONE;

    for (BigInteger i = BigInteger.TWO; i.compareTo(x) <= 0; i = i.add(BigInteger.ONE)) {
      result = result.multiply(i);
    }

    return result;
  }
}
