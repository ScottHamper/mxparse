package com.scotthamper.mxparse;

import java.util.function.Function;

public record UnaryExpression<T>(Function<T, T> function, Expression<T> operand)
    implements Expression<T> {
  @Override
  public T evaluate() {
    return function.apply(operand.evaluate());
  }
}
