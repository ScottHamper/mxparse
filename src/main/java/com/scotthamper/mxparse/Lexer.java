package com.scotthamper.mxparse;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Lexer {
  public static final Map<Token.Type, String> DEFAULT_TOKEN_TYPE_REGEXES =
      Map.ofEntries(
          Map.entry(Token.Type.NUMBER, "\\d+"),
          Map.entry(Token.Type.WHITESPACE, "\\s+"),
          Map.entry(Token.Type.GROUP_START, "\\("),
          Map.entry(Token.Type.GROUP_END, "\\)"),
          Map.entry(Token.Type.BITWISE_COMPLEMENT, "~"),
          Map.entry(Token.Type.FACTORIAL, "!"),
          Map.entry(Token.Type.EXPONENTIATION, "\\*{2}"),
          Map.entry(Token.Type.TIMES, "(?<=^|[^*]|\\*{2})\\*(?=[^*]|\\*{2}|$)"),
          Map.entry(Token.Type.DIVIDE, "/"),
          Map.entry(Token.Type.MODULO, "%"),
          Map.entry(Token.Type.PLUS, "\\+"),
          Map.entry(Token.Type.MINUS, "-"),
          Map.entry(Token.Type.BITWISE_AND, "&"),
          Map.entry(Token.Type.BITWISE_XOR, "\\^"),
          Map.entry(Token.Type.BITWISE_OR, "\\|"));

  private final Map<Token.Type, Pattern> tokenTypePatterns;

  public Lexer() {
    this(DEFAULT_TOKEN_TYPE_REGEXES);
  }

  public Lexer(Map<Token.Type, String> tokenTypeRegexes) {
    this.tokenTypePatterns =
        tokenTypeRegexes.entrySet().stream()
            .collect(
                Collectors.toUnmodifiableMap(
                    Map.Entry::getKey, e -> Pattern.compile(e.getValue())));
  }

  // FIXME: Return useful info on error, rather than throw exceptions.
  public Iterable<Token> tokenize(String input) {
    return () ->
        new Iterator<>() {
          private int cursor = 0;

          @Override
          public boolean hasNext() {
            return cursor < input.length();
          }

          @Override
          public Token next() {
            if (!hasNext()) {
              throw new NoSuchElementException();
            }

            // Deliberately avoiding the Stream API here in the name of performance.
            for (Token.Type type : tokenTypePatterns.keySet()) {
              Matcher matcher = tokenTypePatterns.get(type).matcher(input);

              if (matcher.find(cursor) && matcher.start() == cursor) {
                cursor = matcher.end();

                return new Token(type, matcher.group());
              }
            }

            throw new IllegalStateException(
                String.format("Unknown token at index %s: %s", cursor, input.charAt(cursor)));
          }
        };
  }
}
