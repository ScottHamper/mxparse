package com.scotthamper.mxparse;

public record Token(Type type, String lexeme) {
  public record Type(String name) {
    public static final Type NUMBER = new Type("Number");
    public static final Type GROUP_START = new Type("Group Start");
    public static final Type GROUP_END = new Type("Group End");
    public static final Type BITWISE_COMPLEMENT = new Type("Bitwise Complement");
    public static final Type FACTORIAL = new Type("Factorial");
    public static final Type EXPONENTIATION = new Type("Exponentiation");
    public static final Type TIMES = new Type("Times");
    public static final Type DIVIDE = new Type("Divide");
    public static final Type MODULO = new Type("Modulo");
    public static final Type PLUS = new Type("Plus");
    public static final Type MINUS = new Type("Minus");
    public static final Type BITWISE_AND = new Type("Bitwise AND");
    public static final Type BITWISE_XOR = new Type("Bitwise XOR");
    public static final Type BITWISE_OR = new Type("Bitwise OR");
    public static final Type WHITESPACE = new Type("Whitespace");
  }
}
