package com.scotthamper.mxparse;

import java.util.function.BiFunction;

public record BinaryExpression<T>(
    BiFunction<T, T, T> function, Expression<T> leftOperand, Expression<T> rightOperand)
    implements Expression<T> {
  @Override
  public T evaluate() {
    return function.apply(leftOperand.evaluate(), rightOperand.evaluate());
  }
}
