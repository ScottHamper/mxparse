package com.scotthamper.mxparse;

public record ValueExpression<T>(T value) implements Expression<T> {
  @Override
  public T evaluate() {
    return value;
  }
}
