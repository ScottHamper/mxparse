package com.scotthamper.mxparse;

public interface Expression<T> {
  T evaluate();
}
