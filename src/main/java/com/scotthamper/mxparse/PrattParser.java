package com.scotthamper.mxparse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

// See https://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy/ for
// background.
public class PrattParser<T> {
  public static final int MIN_BINDING_POWER = 0;

  private final Map<Token.Type, HeadSubParser<T>> headSubParsers;
  private final Map<Token.Type, TailSubParser<T>> tailSubParsers;
  private final Map<Token.Type, Integer> leftBindingPowers;
  private final Set<Token.Type> ignored;

  private PrattParser(
      Map<Token.Type, HeadSubParser<T>> headSubParsers,
      Map<Token.Type, TailSubParser<T>> tailSubParsers,
      Map<Token.Type, Integer> leftBindingPowers,
      Set<Token.Type> ignored) {
    this.headSubParsers = Map.copyOf(headSubParsers);
    this.tailSubParsers = Map.copyOf(tailSubParsers);
    this.leftBindingPowers = Map.copyOf(leftBindingPowers);
    this.ignored = Set.copyOf(ignored);
  }

  public Expression<T> parse(Iterable<Token> tokens) {
    var iterator = new TokenIterator(ignored, tokens.iterator());
    Expression<T> expression = parse(iterator, MIN_BINDING_POWER);

    // There should not be any tokens remaining after the parser has finished. One situation where
    // this can occur is when a group close token does not have a matching start token. The close
    // token only gets consumed via the sub-parser of its matching start token. Without the start
    // token, not only will the close token not be consumed, but everything to its left will be
    // returned as the final result of the recursive `parse` method, even if there are more tokens
    // after it.
    if (iterator.hasNext()) {
      throw new IllegalArgumentException();
    }

    return expression;
  }

  public Expression<T> parse(TokenIterator tokens, int rightBindingPower) {
    if (!tokens.hasNext()) {
      throw new IllegalArgumentException("Unexpected end of expression.");
    }

    Token token = tokens.next();

    if (!headSubParsers.containsKey(token.type())) {
      throw new IllegalArgumentException("Invalid start of (sub-)expression: " + token.lexeme());
    }

    Expression<T> leftOperand = headSubParsers.get(token.type()).apply(this, tokens);

    while (true) {
      if (!tokens.hasNext()) {
        return leftOperand;
      }

      Token nextToken = tokens.peekNext();

      if (!leftBindingPowers.containsKey(nextToken.type())) {
        throw new IllegalArgumentException(
            "Unknown left binding power for token: " + nextToken.lexeme());
      }

      if (leftBindingPowers.get(nextToken.type()) <= rightBindingPower) {
        return leftOperand;
      }

      if (!tailSubParsers.containsKey(nextToken.type())) {
        throw new IllegalArgumentException(
            "No tail sub-parser specified for token: " + nextToken.lexeme());
      }

      leftOperand = tailSubParsers.get(tokens.next().type()).apply(this, tokens, leftOperand);
    }
  }

  // This class decorates an existing iterator with additional methods that are useful for the
  // parser, such as peeking the next token. It follows the strategy discussed in section 2.2 of
  // https://abarker.github.io/typped/pratt_parsing_intro.html in order to "fake [an iterator] with
  // one-token `peek` lookahead from [an iterator] without built-in lookahead." This class also
  // handles skipping over tokens that have an ignored type.
  public static final class TokenIterator implements Iterator<Token> {
    private final Set<Token.Type> ignored;
    private final Iterator<Token> tokens;
    private Token current;
    private Token next;

    private TokenIterator(Set<Token.Type> ignored, Iterator<Token> tokens) {
      this.ignored = ignored;
      this.tokens = tokens;

      // This initial call to `nextOrNull` will populate `next` with the first token (if there is
      // one), so that the `peekNext` method has something to return.
      nextOrNull();
    }

    private Token nextOrNull() {
      current = next;
      next = null;

      while (tokens.hasNext()) {
        Token n = tokens.next();

        if (!ignored.contains(n.type())) {
          next = n;
          break;
        }
      }

      return current;
    }

    public Token current() {
      if (current == null) {
        throw new NoSuchElementException();
      }

      return current;
    }

    @Override
    public boolean hasNext() {
      return next != null;
    }

    public Token peekNext() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }

      return next;
    }

    @Override
    public Token next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }

      return nextOrNull();
    }

    public void consume(Token.Type type) {
      if (!hasNext() || next.type() != type) {
        throw new NoSuchElementException();
      }

      nextOrNull();
    }
  }

  // Who says Java doesn't have type aliases?
  // OK, so it's not *really* a type alias... The downside of this approach is that a variable typed
  // as a BiFunction<...> cannot be passed in for a HeadSubParser. However, lambdas and method
  // references work seamlessly. In the situation with variables, it *is* shorter to write
  // `PrattParser.HeadSubParser<T>` than it is to write out the entire BiFunction type definition,
  // and it provides better context. It's only really a problem if there are BiFunction variables
  // lying around whose type definitions can't be modified (e.g., contained in another library) and
  // that want to be used with the parser. Given that the BiFunction has a PrattParser and
  // TokenIterator parameter, I can 100% guarantee at the time of this writing that such a situation
  // does not exist, and any variables created in the future will have to be typed as HeadSubParser.
  public interface HeadSubParser<T>
      extends BiFunction<PrattParser<T>, TokenIterator, Expression<T>> {}

  // Same caveat as with the HeadSubParser.
  public interface TailSubParser<T>
      extends Function3<PrattParser<T>, TokenIterator, Expression<T>, Expression<T>> {}

  public static class Builder<T> {
    private final Map<Token.Type, HeadSubParser<T>> headSubParsers;
    private final Map<Token.Type, TailSubParser<T>> tailSubParsers;
    private final Map<Token.Type, Integer> leftBindingPowers;
    private final Set<Token.Type> ignored;

    public Builder() {
      headSubParsers = new HashMap<>();
      tailSubParsers = new HashMap<>();
      leftBindingPowers = new HashMap<>();
      ignored = new HashSet<>();
    }

    public Builder<T> withHead(Token.Type type, HeadSubParser<T> subParser) {
      headSubParsers.put(type, subParser);
      return this;
    }

    public Builder<T> withTail(Token.Type type, int bindingPower, TailSubParser<T> subParser) {
      if (bindingPower <= MIN_BINDING_POWER) {
        throw new IllegalArgumentException();
      }

      leftBindingPowers.put(type, bindingPower);
      tailSubParsers.put(type, subParser);
      return this;
    }

    public Builder<T> withConsumed(Token.Type type) {
      leftBindingPowers.put(type, MIN_BINDING_POWER);
      return this;
    }

    public Builder<T> withLiteral(Token.Type type, Function<String, T> lexemeParser) {
      return withHead(
          type,
          (parser, tokens) -> new ValueExpression<>(lexemeParser.apply(tokens.current().lexeme())));
    }

    public Builder<T> withPrefix(Token.Type type, int bindingPower, Function<T, T> function) {
      if (bindingPower <= MIN_BINDING_POWER) {
        throw new IllegalArgumentException();
      }

      return withHead(
          type,
          (parser, tokens) ->
              new UnaryExpression<>(function, parser.parse(tokens, bindingPower - 1)));
    }

    public Builder<T> withPostfix(Token.Type type, int bindingPower, Function<T, T> function) {
      if (bindingPower <= MIN_BINDING_POWER) {
        throw new IllegalArgumentException();
      }

      return withTail(
          type,
          bindingPower,
          (parser, tokens, leftOperand) -> new UnaryExpression<>(function, leftOperand));
    }

    public Builder<T> withInfix(Token.Type type, int bindingPower, BiFunction<T, T, T> function) {
      return withInfix(type, bindingPower, function, false);
    }

    public Builder<T> withRightInfix(
        Token.Type type, int bindingPower, BiFunction<T, T, T> function) {
      return withInfix(type, bindingPower, function, true);
    }

    private Builder<T> withInfix(
        Token.Type type,
        int bindingPower,
        BiFunction<T, T, T> function,
        boolean isRightAssociative) {
      return withTail(
          type,
          bindingPower,
          (parser, tokens, leftOperand) ->
              new BinaryExpression<>(
                  function,
                  leftOperand,
                  parser.parse(tokens, bindingPower - (isRightAssociative ? 1 : 0))));
    }

    public Builder<T> withGrouping(Token.Type startType, Token.Type endType) {
      return withConsumed(endType)
          .withHead(
              startType,
              (parser, tokens) -> {
                Expression<T> right = parser.parse(tokens, MIN_BINDING_POWER);
                tokens.consume(endType);

                return right;
              });
    }

    public Builder<T> ignoring(Token.Type... types) {
      ignored.addAll(List.of(types));
      return this;
    }

    public PrattParser<T> instance() {
      return new PrattParser<>(headSubParsers, tailSubParsers, leftBindingPowers, ignored);
    }
  }
}
