package com.scotthamper.mxparse;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

public class PostfixParser<T> {
  private final Map<Token.Type, Function<String, T>> literalParsers;
  private final Map<Token.Type, Function<T, T>> unaryFunctions;
  private final Map<Token.Type, BiFunction<T, T, T>> binaryFunctions;
  private final Set<Token.Type> ignored;

  private PostfixParser(
      Map<Token.Type, Function<String, T>> literalParsers,
      Map<Token.Type, Function<T, T>> unaryFunctions,
      Map<Token.Type, BiFunction<T, T, T>> binaryFunctions,
      Set<Token.Type> ignored) {
    this.literalParsers = Map.copyOf(literalParsers);
    this.unaryFunctions = Map.copyOf(unaryFunctions);
    this.binaryFunctions = Map.copyOf(binaryFunctions);
    this.ignored = Set.copyOf(ignored);
  }

  public Expression<T> parse(Iterable<Token> tokens) {
    Deque<Expression<T>> operands = new ArrayDeque<>();

    for (Token token : tokens) {
      if (literalParsers.containsKey(token.type())) {
        T value = literalParsers.get(token.type()).apply(token.lexeme());

        operands.add(new ValueExpression<>(value));
      } else if (unaryFunctions.containsKey(token.type())) {
        if (operands.size() < 1) {
          throw new IllegalArgumentException(
              "Missing operand for unary operator: " + token.lexeme());
        }

        Function<T, T> function = unaryFunctions.get(token.type());
        Expression<T> operand = operands.removeLast();

        operands.add(new UnaryExpression<>(function, operand));
      } else if (binaryFunctions.containsKey(token.type())) {
        if (operands.size() < 2) {
          throw new IllegalArgumentException(
              "Missing operand(s) for binary operator: " + token.lexeme());
        }

        BiFunction<T, T, T> function = binaryFunctions.get(token.type());
        Expression<T> rightOperand = operands.removeLast();
        Expression<T> leftOperand = operands.removeLast();

        operands.add(new BinaryExpression<>(function, leftOperand, rightOperand));
      } else if (!ignored.contains(token.type())) {
        throw new IllegalArgumentException(token.toString());
      }
    }

    if (operands.size() > 1) {
      throw new IllegalArgumentException("Expression contained too many operands.");
    } else if (operands.size() < 1) {
      throw new IllegalArgumentException("Expression is empty or consists only of whitespace.");
    }

    return operands.removeLast();
  }

  public static class Builder<T> {
    private final Map<Token.Type, Function<String, T>> literalParsers;
    private final Map<Token.Type, Function<T, T>> unaryFunctions;
    private final Map<Token.Type, BiFunction<T, T, T>> binaryFunctions;
    private final Set<Token.Type> ignored;

    public Builder() {
      literalParsers = new HashMap<>();
      unaryFunctions = new HashMap<>();
      binaryFunctions = new HashMap<>();
      ignored = new HashSet<>();
    }

    public Builder<T> withLiteral(Token.Type type, Function<String, T> function) {
      literalParsers.put(type, function);
      return this;
    }

    public Builder<T> withUnary(Token.Type type, Function<T, T> function) {
      unaryFunctions.put(type, function);
      return this;
    }

    public Builder<T> withBinary(Token.Type type, BiFunction<T, T, T> function) {
      binaryFunctions.put(type, function);
      return this;
    }

    public Builder<T> ignoring(Token.Type... types) {
      ignored.addAll(List.of(types));
      return this;
    }

    public PostfixParser<T> instance() {
      return new PostfixParser<>(literalParsers, unaryFunctions, binaryFunctions, ignored);
    }
  }
}
