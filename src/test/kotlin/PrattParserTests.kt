package com.scotthamper.mxparse

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.function.BiFunction

class PrattParserTests {
    private val literalType = Token.Type("Literal")
    private val groupStartType = Token.Type("Group Start")
    private val groupEndType = Token.Type("Group End")
    private val strongPrefixOperatorType = Token.Type("Strong Prefix Operator")
    private val weakPrefixOperatorType = Token.Type("Weak Prefix Operator")
    private val strongPostfixOperatorType = Token.Type("Strong Postfix Operator")
    private val weakPostfixOperatorType = Token.Type("Weak Postfix Operator")
    private val rightInfixOperatorType = Token.Type("Right Associative Infix Operator")
    private val strongInfixOperatorType = Token.Type("Strong Infix Operator")
    private val weakInfixOperatorType = Token.Type("Weak Infix Operator")
    private val ignoredType = Token.Type("Ignored")

    private val strongPrefixFunction = JFunction<Int, Int> { 0 }
    private val weakPrefixFunction = JFunction<Int, Int> { 0 }
    private val strongPostfixFunction = JFunction<Int, Int> { 0 }
    private val weakPostfixFunction = JFunction<Int, Int> { 0 }
    private val rightBinaryFunction = BiFunction<Int, Int, Int> { _, _ -> 0 }
    private val strongBinaryFunction = BiFunction<Int, Int, Int> { _, _ -> 0 }
    private val weakBinaryFunction = BiFunction<Int, Int, Int> { _, _ -> 0 }

    private val parser = PrattParser.Builder<Int>()
        .withLiteral(literalType, String::toInt)
        .withGrouping(groupStartType, groupEndType)
        .withPrefix(strongPrefixOperatorType, 6, strongPrefixFunction)
        .withPrefix(weakPrefixOperatorType, 1, weakPrefixFunction)
        .withPostfix(strongPostfixOperatorType, 6, strongPostfixFunction)
        .withPostfix(weakPostfixOperatorType, 1, weakPostfixFunction)
        .withRightInfix(rightInfixOperatorType, 6, rightBinaryFunction)
        .withInfix(strongInfixOperatorType, 5, strongBinaryFunction)
        .withInfix(weakInfixOperatorType, 4, weakBinaryFunction)
        .ignoring(ignoredType)
        .instance()

    private fun Int.toLiteralToken() = Token(literalType, this.toString())

    @Nested
    inner class Parse {
        @Test
        fun `constructs expression tree when given a literal token`() =
            parser.parse(listOf(5.toLiteralToken())) shouldBe ValueExpression(
                5
            )

        @Test
        fun `ignores tokens that have a type ignored by the parser`() =
            parser.parse(
                listOf(
                    Token(ignoredType, " "),
                    5.toLiteralToken(),
                    Token(ignoredType, " "),
                )
            ) shouldBe ValueExpression(5)

        @Test
        fun `constructs expression tree when given a prefix token and a literal token`() =
            parser.parse(
                listOf(
                    Token(strongPrefixOperatorType, "~"),
                    5.toLiteralToken(),
                )
            ) shouldBe UnaryExpression(
                strongPrefixFunction,
                ValueExpression(5),
            )

        @Test
        fun `constructs expression tree when given two literal tokens and an infix token`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                )
            ) shouldBe BinaryExpression(
                weakBinaryFunction,
                ValueExpression(5),
                ValueExpression(13),
            )

        @Test
        fun `gives a prefix token precedence over a postfix token when its binding power is higher`() =
            parser.parse(
                listOf(
                    Token(strongPrefixOperatorType, "~"),
                    5.toLiteralToken(),
                    Token(weakPostfixOperatorType, "!"),
                )
            ) shouldBe UnaryExpression(
                weakPostfixFunction,
                UnaryExpression(
                    strongPrefixFunction,
                    ValueExpression(5),
                ),
            )

        @Test
        fun `gives a prefix token precedence over an infix token when its binding power is higher`() =
            parser.parse(
                listOf(
                    Token(strongPrefixOperatorType, "~"),
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                )
            ) shouldBe BinaryExpression(
                weakBinaryFunction,
                UnaryExpression(
                    strongPrefixFunction,
                    ValueExpression(5),
                ),
                ValueExpression(13),
            )

        @Test
        fun `gives a postfix token precedence over a prefix token when its binding power is higher`() =
            parser.parse(
                listOf(
                    Token(weakPrefixOperatorType, "~"),
                    5.toLiteralToken(),
                    Token(strongPostfixOperatorType, "!"),
                )
            ) shouldBe UnaryExpression(
                weakPrefixFunction,
                UnaryExpression(
                    strongPostfixFunction,
                    ValueExpression(5),
                ),
            )

        @Test
        fun `gives a postfix token precedence over a prefix token when its binding power is the same`() =
            parser.parse(
                listOf(
                    Token(strongPrefixOperatorType, "~"),
                    5.toLiteralToken(),
                    Token(strongPostfixOperatorType, "!"),
                )
            ) shouldBe UnaryExpression(
                strongPrefixFunction,
                UnaryExpression(
                    strongPostfixFunction,
                    ValueExpression(5),
                ),
            )

        @Test
        fun `gives a postfix token precedence over an infix token when its binding power is higher`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                    Token(strongPostfixOperatorType, "!"),
                )
            ) shouldBe BinaryExpression(
                weakBinaryFunction,
                ValueExpression(5),
                UnaryExpression(
                    strongPostfixFunction,
                    ValueExpression(13),
                ),
            )

        @Test
        fun `gives an infix token precedence over a prefix token when its binding power is higher`() =
            parser.parse(
                listOf(
                    Token(weakPrefixOperatorType, "~"),
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                )
            ) shouldBe UnaryExpression(
                weakPrefixFunction,
                BinaryExpression(
                    weakBinaryFunction,
                    ValueExpression(5),
                    ValueExpression(13),
                ),
            )

        @Test
        fun `gives an infix token precedence over a postfix token when its binding power is higher`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                    Token(weakPostfixOperatorType, "!"),
                )
            ) shouldBe UnaryExpression(
                weakPostfixFunction,
                BinaryExpression(
                    weakBinaryFunction,
                    ValueExpression(5),
                    ValueExpression(13),
                ),
            )

        @Test
        fun `gives an infix token precedence over another when its binding power is higher`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                    Token(strongInfixOperatorType, "*"),
                    9001.toLiteralToken(),
                )
            ) shouldBe BinaryExpression(
                weakBinaryFunction,
                ValueExpression(5),
                BinaryExpression(
                    strongBinaryFunction,
                    ValueExpression(13),
                    ValueExpression(9001),
                ),
            )

        @Test
        fun `combines grouped expressions`() {
            parser.parse(
                listOf(
                    Token(groupStartType, "("),
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                    Token(groupEndType, ")"),
                    Token(strongInfixOperatorType, "*"),
                    9001.toLiteralToken(),
                )
            ) shouldBe BinaryExpression(
                strongBinaryFunction,
                BinaryExpression(
                    weakBinaryFunction,
                    ValueExpression(5),
                    ValueExpression(13),
                ),
                ValueExpression(9001),
            )
        }

        @Test
        fun `parses left-associative infix tokens that have the same precedence level from left to right`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    13.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    9001.toLiteralToken(),
                )
            ) shouldBe BinaryExpression(
                weakBinaryFunction,
                BinaryExpression(
                    weakBinaryFunction,
                    ValueExpression(5),
                    ValueExpression(13),
                ),
                ValueExpression(9001),
            )

        @Test
        fun `parses right-associative infix tokens that have the same precedence level from right to left`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(rightInfixOperatorType, "**"),
                    13.toLiteralToken(),
                    Token(rightInfixOperatorType, "**"),
                    9001.toLiteralToken(),
                )
            ) shouldBe BinaryExpression(
                rightBinaryFunction,
                ValueExpression(5),
                BinaryExpression(
                    rightBinaryFunction,
                    ValueExpression(13),
                    ValueExpression(9001),
                ),
            )

        @Test
        fun `throws when there are too many literal tokens`() {
            listOf(
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                ),
                listOf(
                    5.toLiteralToken(),
                    Token(strongPrefixOperatorType, "~"),
                    13.toLiteralToken(),
                ),
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                    Token(strongPostfixOperatorType, "!"),
                ),
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                    Token(weakInfixOperatorType, "+"),
                    9001.toLiteralToken(),
                )
            ).forAll { tokens ->
                shouldThrow<IllegalArgumentException> { parser.parse(tokens) }
            }
        }

        @Test
        fun `throws when given an empty iterable`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf())
            }
        }

        @Test
        fun `throws when only given ignored tokens`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(ignoredType, " ")))
            }
        }

        @Test
        fun `throws when there are not enough literal tokens for a prefix token`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(strongPrefixOperatorType, "~")))
            }
        }

        @Test
        fun `throws when there are not enough literal tokens for a postfix token`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(strongPostfixOperatorType, "!")))
            }
        }

        @Test
        fun `throws when there are not enough literal tokens for an infix token`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(
                    listOf(
                        5.toLiteralToken(),
                        Token(weakInfixOperatorType, "+"),
                    )
                )
            }
        }

        @Test
        fun `throws when there is an unmatched group start token`() {
            // Currently a `NoSuchElementException` rather than an `IllegalArgumentException`
            // because it's the `TokenIterator.consume` method that throws when the sub-parser for
            // the open token tries to consume a close token that doesn't exist.
            shouldThrow<NoSuchElementException> {
                parser.parse(
                    listOf(
                        Token(groupStartType, "("),
                        5.toLiteralToken(),
                    )
                )
            }
        }

        @Test
        fun `throws when there is an unmatched group end token`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(
                    listOf(
                        5.toLiteralToken(),
                        Token(groupEndType, ")"),
                    )
                )
            }
        }

        @Test
        fun `throws when encountering an unsupported token`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(Token.Type("Unsupported"), "@")))
            }
        }
    }

    @Nested
    inner class BuilderTests {
        @Nested
        inner class WithTail {
            @Test
            fun `throws when given too small of a binding power`() {
                listOf(
                    PrattParser.MIN_BINDING_POWER,
                    PrattParser.MIN_BINDING_POWER - 1,
                ).forAll { bindingPower ->
                    shouldThrow<IllegalArgumentException> {
                        PrattParser.Builder<Int>()
                            .withTail(Token.Type(""), bindingPower) { _, _, l -> l }
                    }
                }
            }
        }

        @Nested
        inner class WithPrefix {
            @Test
            fun `throws when given too small of a binding power`() {
                listOf(
                    PrattParser.MIN_BINDING_POWER,
                    PrattParser.MIN_BINDING_POWER - 1,
                ).forAll { bindingPower ->
                    shouldThrow<IllegalArgumentException> {
                        PrattParser.Builder<Int>()
                            .withPrefix(Token.Type(""), bindingPower) { 5 }
                    }
                }
            }
        }

        @Nested
        inner class WithPostfix {
            @Test
            fun `throws when given too small of a binding power`() {
                listOf(
                    PrattParser.MIN_BINDING_POWER,
                    PrattParser.MIN_BINDING_POWER - 1,
                ).forAll { bindingPower ->
                    shouldThrow<IllegalArgumentException> {
                        PrattParser.Builder<Int>()
                            .withPostfix(Token.Type(""), bindingPower) { 5 }
                    }
                }
            }
        }
    }
}
