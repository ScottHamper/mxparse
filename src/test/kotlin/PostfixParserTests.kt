package com.scotthamper.mxparse

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.function.BiFunction

private typealias JFunction<T, R> = java.util.function.Function<T, R>

class PostfixParserTests {
    private val valueType = Token.Type("Value")
    private val unaryOperatorType = Token.Type("Unary Operator")
    private val binaryOperatorType = Token.Type("Binary Operator")
    private val ignoredType = Token.Type("Ignored")

    private val unaryFunction = JFunction<Int, Int> { 0 }
    private val binaryFunction = BiFunction<Int, Int, Int> { _, _ -> 0 }

    private val parser = PostfixParser.Builder<Int>()
        .withLiteral(valueType) { lexeme -> lexeme.toInt() }
        .withUnary(unaryOperatorType, unaryFunction)
        .withBinary(binaryOperatorType, binaryFunction)
        .ignoring(ignoredType)
        .instance()

    private fun Int.toLiteralToken() = Token(valueType, this.toString())

    @Nested
    inner class Parse {
        @Test
        fun `constructs expression tree when given a literal token`() {
            listOf(
                5,
                13,
                9001,
            ).forAll { int ->
                parser.parse(listOf(int.toLiteralToken())) shouldBe ValueExpression(
                    int
                )
            }
        }

        @Test
        fun `ignores tokens that have a type ignored by the parser`() =
            parser.parse(
                listOf(
                    Token(ignoredType, " "),
                    5.toLiteralToken(),
                    Token(ignoredType, " "),
                )
            ) shouldBe ValueExpression(5)

        @Test
        fun `constructs expression tree when given a literal token and a unary operator token`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    Token(unaryOperatorType, "~"),
                )
            ) shouldBe UnaryExpression(
                unaryFunction,
                ValueExpression(5),
            )

        @Test
        fun `constructs expression tree when given two literal tokens and a binary operator token`() =
            parser.parse(
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                    Token(binaryOperatorType, "+"),
                )
            ) shouldBe BinaryExpression(
                binaryFunction,
                ValueExpression(5),
                ValueExpression(13),
            )

        @Test
        fun `throws when there are too many literal tokens`() {
            listOf(
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                ),
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                    Token(unaryOperatorType, "~"),
                ),
                listOf(
                    5.toLiteralToken(),
                    13.toLiteralToken(),
                    9001.toLiteralToken(),
                    Token(binaryOperatorType, "+"),
                )
            ).forAll { tokens ->
                shouldThrow<IllegalArgumentException> { parser.parse(tokens) }
            }
        }

        @Test
        fun `throws when given an empty iterable`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf())
            }
        }

        @Test
        fun `throws when only given ignored tokens`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(ignoredType, " ")))
            }
        }

        @Test
        fun `throws when there are not enough literal tokens for a unary operator`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(unaryOperatorType, "~")))
            }
        }

        @Test
        fun `throws when there are not enough literal tokens for a binary operator`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(
                    listOf(
                        5.toLiteralToken(),
                        Token(binaryOperatorType, "+"),
                    )
                )
            }
        }

        @Test
        fun `throws when encountering an unsupported token`() {
            shouldThrow<IllegalArgumentException> {
                parser.parse(listOf(Token(Token.Type("Unsupported"), "@")))
            }
        }
    }
}
