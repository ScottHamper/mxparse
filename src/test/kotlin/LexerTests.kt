package com.scotthamper.mxparse

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class LexerTests {
    @Nested
    inner class Tokenize {
        @Test
        fun `returns known default tokens in the order they appear`() =
            Lexer().tokenize("~(3! +\t5)/2%4\r*6**\n7-1&8^9|0").toList() shouldBe listOf(
                Token(Token.Type.BITWISE_COMPLEMENT, "~"),
                Token(Token.Type.GROUP_START, "("),
                Token(Token.Type.NUMBER, "3"),
                Token(Token.Type.FACTORIAL, "!"),
                Token(Token.Type.WHITESPACE, " "),
                Token(Token.Type.PLUS, "+"),
                Token(Token.Type.WHITESPACE, "\t"),
                Token(Token.Type.NUMBER, "5"),
                Token(Token.Type.GROUP_END, ")"),
                Token(Token.Type.DIVIDE, "/"),
                Token(Token.Type.NUMBER, "2"),
                Token(Token.Type.MODULO, "%"),
                Token(Token.Type.NUMBER, "4"),
                Token(Token.Type.WHITESPACE, "\r"),
                Token(Token.Type.TIMES, "*"),
                Token(Token.Type.NUMBER, "6"),
                Token(Token.Type.EXPONENTIATION, "**"),
                Token(Token.Type.WHITESPACE, "\n"),
                Token(Token.Type.NUMBER, "7"),
                Token(Token.Type.MINUS, "-"),
                Token(Token.Type.NUMBER, "1"),
                Token(Token.Type.BITWISE_AND, "&"),
                Token(Token.Type.NUMBER, "8"),
                Token(Token.Type.BITWISE_XOR, "^"),
                Token(Token.Type.NUMBER, "9"),
                Token(Token.Type.BITWISE_OR, "|"),
                Token(Token.Type.NUMBER, "0"),
            )

        @Test
        fun `recognizes the times operator at the start of the input`() =
            Lexer().tokenize("*5").first().type shouldBe Token.Type.TIMES

        @Test
        fun `recognizes the times operator at the end of the input`() =
            Lexer().tokenize("5*").last().type shouldBe Token.Type.TIMES

        @Test
        fun `groups consecutive digits into a single IntLiteral token`() =
            Lexer().tokenize("4128").toList() shouldBe listOf(
                Token(Token.Type.NUMBER, "4128")
            )

        @Test
        fun `groups consecutive whitespace characters into a single Whitespace token`() =
            Lexer().tokenize(" \t\r\n").toList() shouldBe listOf(
                Token(Token.Type.WHITESPACE, " \t\r\n")
            )

        @Test
        fun `throws when encountering an unknown token`() {
            listOf("$-5", "-$5", "-5$").forAll { input ->
                shouldThrow<IllegalStateException> {
                    Lexer().tokenize(input).toList()
                }
            }
        }
    }
}
