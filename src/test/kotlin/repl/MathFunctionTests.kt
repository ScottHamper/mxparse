package com.scotthamper.mxparse.repl

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class MathFunctionTests {
    @Nested
    inner class Factorial {
        @Test
        fun `is one when given zero`() =
            MathFunction.factorial(0.toBigInteger()) shouldBe 1.toBigInteger()

        @Test
        fun `is the product of all numbers from one to x, inclusive`() {
            listOf(
                1.toBigInteger() to 1.toBigInteger(),
                4.toBigInteger() to 24.toBigInteger(),
                9.toBigInteger() to 362880.toBigInteger()
            ).forAll { (x, expectedResult) ->
                MathFunction.factorial(x) shouldBe expectedResult
            }
        }

        @Test
        fun `throws when given a negative number`() {
            shouldThrow<IllegalArgumentException> { MathFunction.factorial((-4).toBigInteger()) }
        }
    }
}
