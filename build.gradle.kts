import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    java
    kotlin("jvm") version "1.6.21"
    id("com.diffplug.spotless") version "6.6.1"
    id("com.github.spotbugs") version "5.0.7"
}

repositories {
    mavenCentral()
}

group = "com.scotthamper"
version = "1.0-SNAPSHOT"

val run: JavaExec by tasks
run.standardInput = System.`in`

val main = "com.scotthamper.mxparse.repl.Main"

application {
    mainClass.set(main)
    applicationDefaultJvmArgs = listOf("-Dfile.encoding=UTF-8")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(16))
    }
}

spotbugs {
    excludeFilter.set(file("spotbugs-exclude.xml"))
}

spotless {
    java {
        googleJavaFormat().reflowLongStrings()
    }

    kotlin {
        ktlint("0.43.2")
    }
}

tasks {
    test {
        useJUnitPlatform()
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "16"
    }

    withType<Jar> {
        manifest {
            attributes["Main-Class"] = main
        }
    }

    // These two settings allow for reproducible JAR builds. See:
    // https://docs.gradle.org/current/userguide/working_with_files.html#sec:reproducible_archives
    withType<AbstractArchiveTask>().configureEach {
        isPreserveFileTimestamps = false
        isReproducibleFileOrder = true
    }
}

dependencies {
    testImplementation(kotlin("test"))
    testImplementation("io.kotest:kotest-assertions-core:5.3.0")
    spotbugsPlugins("com.h3xstream.findsecbugs:findsecbugs-plugin:1.12.0")
}
